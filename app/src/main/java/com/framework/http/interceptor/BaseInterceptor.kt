package com.framework.http.interceptor

import android.text.TextUtils
import android.util.Log
import com.framework.http.interfac.NetworkRequestParamsListener
import com.google.gson.Gson
import okhttp3.*
import java.io.IOException
import java.util.*

/**
 * @author: xiaxueyi
 * @date: 2022-08-25
 * @time: 15:07
 * @说明: 自定义请求头,上传到服务器的参数在这里统一做处理
 */
class BaseInterceptor constructor( private val mNetworkRequestParamsListener: NetworkRequestParamsListener?) : Interceptor {


    private val TimeOut: Int = 120 //连接超时时间和读写超时时间 120秒


    @Throws(IOException::class)
    public override fun intercept(chain: Interceptor.Chain): Response {
        //获取请求的对象
        val request: Request = chain.request()
        //获取Builder 实例
        val builder: Request.Builder = request.newBuilder()
        val url: HttpUrl = request.url
        val apiPath: String = url.encodedPath.trim { it <= ' ' }

        if(mNetworkRequestParamsListener!=null){
            val headerMap: MutableMap<String, Any> = mNetworkRequestParamsListener.getHeaderParams()
            if (headerMap.isNotEmpty()) {
                //获取集合
                val keys: Set<String?> = headerMap.keys
                //迭代集合，输出
                val iterator: Iterator<*> = keys.iterator()
                while (iterator.hasNext()) {
                    //获取到Key
                    val headerKey: String = iterator.next() as String
                    //根据Key获取到值
                    val value: String = Objects.requireNonNull(headerMap.get(headerKey)).toString()
                    if (value.isNotEmpty()) {
                        builder.addHeader(headerKey, value)
                    }
                }
                Log.e("","okHttp--->>header: " + Gson().toJson(headerMap))
            }
        }

        val response: Response = chain.proceed(builder.build())
        try {
            //获取response的header 特殊处理 head 请求，回调到body
            val key: String? = response.header("key")
            if (!TextUtils.isEmpty(key)) {
                assert(response.body != null)
                val contentType: MediaType? = response.body!!.contentType()
                val json: String = "{\"success\":true,\"code\":\"200\",\"msg\":\"请求成功\",\"traceId\":\"\",\"data\":\"$key\"}"
                val responseBody: ResponseBody = ResponseBody.create(contentType, json)
                return response.newBuilder().body(responseBody).build()
            }
        } catch (e: Exception) {
        }
        return response
    }
}