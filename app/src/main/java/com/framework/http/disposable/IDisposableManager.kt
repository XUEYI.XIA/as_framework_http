package com.framework.http.disposable

import io.reactivex.rxjava3.disposables.Disposable

/**
 * 调用管理接口定义
 *
 */
interface IDisposableManager {
    /**
     * 添加
     */
    fun addDisposable(tag: Any?, disposable: Disposable?)

    /**
     * 取消/移除
     */
    fun removeDisposable(tag: Any?)

    /**
     * 取消全部
     */
    fun removeAll()

    /**
     * 是否已经处理
     */
    fun isDisposed(tag: Any?): Boolean
}