package com.framework.http.disposable

/**
 * 调用取消接口定义
 *
 */
interface IDisposableCancel {

    fun cancel()

    val isCanceled: Boolean
}