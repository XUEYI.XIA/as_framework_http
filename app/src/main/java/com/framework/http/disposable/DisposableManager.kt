package com.framework.http.disposable

import io.reactivex.rxjava3.disposables.Disposable

/**
 * DisposableManager
 *
 */
class DisposableManager private constructor() : IDisposableManager {


    init {
        mMaps = HashMap()
    }


    override fun addDisposable(tag: Any?, disposable: Disposable?) {
        if (tag == null) return
        mMaps[tag] = disposable
    }


    override fun removeDisposable(tag: Any?) {
        if (tag == null) return
        if (mMaps.isEmpty()) return
        if (mMaps[tag] == null) return
        val disposable = mMaps[tag]
        if (disposable != null && !disposable.isDisposed) disposable.dispose()
        mMaps.remove(tag)
    }

    override fun removeAll() {
        if (mMaps.isEmpty()) return
        //遍历取消请求
        val keySet: Set<Any> = mMaps.keys
        for (key in keySet) {
            removeDisposable(key)
        }
    }

    override fun isDisposed(tag: Any?): Boolean {
        if (tag == null || mMaps.isEmpty()) return true
        val disposable = mMaps[tag]
        return disposable?.isDisposed ?: true
    }

    companion object {
        @Volatile
        private var mInstance: DisposableManager? = null
        private lateinit var mMaps: HashMap<Any, Disposable?>//处理,请求列表

        fun get(): DisposableManager? {
            if (mInstance == null) {
                synchronized(DisposableManager::class.java) {
                    if (mInstance == null) {
                        mInstance = DisposableManager()
                    }
                }
            }
            return mInstance
        }
    }


}