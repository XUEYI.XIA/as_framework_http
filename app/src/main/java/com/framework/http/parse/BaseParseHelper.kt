package com.framework.http.parse


/**
 * @author: xiaxueyi
 * @date: 2022-08-18
 * @time: 14:28
 * @说明: 解析数据接口定义,
 */
interface BaseParseHelper {
    /*解析数据*/
    @Throws(Exception::class)
    fun parse(data:String?)
}