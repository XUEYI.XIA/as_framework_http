package com.framework.http



/**
 * @author: xiaxueyi
 * @date: 2022-08-22
 * @time: 10:13
 * @说明: 响应接口定义，根据接口规范
 */
interface BaseResponse {
    /**
     * 接口逻辑是否成功
     * 备注：例如 code == 0 表示接口处理成功
     *
     * @return
     */
    val isSuccess: Boolean

    /**
     * 接口响应码
     *
     * @return
     */
    val code: Any?

    /**
     * 接口响应描述
     *
     * @return
     */
    val msg: String?
}