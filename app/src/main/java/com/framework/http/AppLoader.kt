package com.framework.http

import android.app.Application
import com.framework.http.interfac.HttpApi
import com.framework.http.interfac.NetworkRequestParamsListener
import com.framework.http.utils.HttpRequestService
import java.util.concurrent.TimeUnit


/**
 * @author: xiaxueyi
 * @date: 2022-08-22
 * @time: 10:13
 * @说明:
 */
class AppLoader :Application() {

    override fun onCreate() {
        super.onCreate()

    }

    /**
     * 初始化网络请求
     */
    init {
        initRHttp();
    }

    /**
     * 初始化网络请求
     */
    private fun initRHttp() {
        val headerMap :MutableMap<String, Any> = mutableMapOf()
        headerMap["Content-Type"] = "application/x-www-form-urlencoded" //默认的编码方式
        headerMap["Connection"] = "Keep-Alive"
        headerMap["Accept-Language"] = "zh-cn"
        headerMap["Accept"] = "Application/Json"

        //必须初始化
        HttpRequestService.Configure.get()
            .baseUrl(HttpApi.BASE_URL) //基础URL
            .timeout(30)
            .timeUnit(TimeUnit.SECONDS)
            .setNetworkRequestParamsListener(object : NetworkRequestParamsListener {

                override fun getHeaderParams(): MutableMap<String, Any> {

                    return headerMap;
                }
            })
            .showLog(true) //日志
            .init(this) //初始化
    }

}