package com.framework.http.observe

import com.framework.http.function.HttpResultFunction
import com.framework.http.function.OnDisposeAction
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.functions.Function
import io.reactivex.rxjava3.schedulers.Schedulers


/**
 * @author: xiaxueyi
 * @date: 2022-08-18
 * @time: 17:27
 * @说明:
 * 网络请求Observable(被监听者)
 * 调度顺序
 * 1.map()
 * 2.onErrorResumeNext()
 * 3.doOnDispose()
 * 4.observe()
 */
class HttpObservable constructor(
    private val apiObservable: Observable<Any>?,
    private val httpObserver: HttpObserver<Any>?) {


    /**
     * Map 操作符，被观察数据再次进行处理，生成新的被观察者，发送到观察者中进行处理，即在HttpObserver回调
     */
    private fun map(): Observable<Any> {
        return apiObservable!!.map(object :Function<Any,Any>{

            override fun apply(t: Any): Any {
//                return GsonBuilder().disableHtmlEscaping().create().toJson(t.toString())
                return t.toString()
            }
        })
    }

    /**
     * onErrorResumeNext
     */
    private fun onErrorResumeNext(): Observable<Any> {
        return map().onErrorResumeNext(HttpResultFunction<Any>())
    }

    /**
     * doOnDispose
     */
    private fun doOnDispose(): Observable<Any> {
        return onErrorResumeNext().doOnDispose(OnDisposeAction(httpObserver))
    }

    /**
     * 线程设置
     */
    fun observe() {
        if (httpObserver != null) {
            doOnDispose()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(httpObserver)
        }
    }
}




