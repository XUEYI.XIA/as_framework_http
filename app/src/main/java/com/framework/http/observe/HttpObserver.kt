package com.framework.http.observe

import android.text.TextUtils
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import com.framework.http.disposable.DisposableManager
import com.framework.http.disposable.IDisposableCancel
import com.framework.http.exception.EventException
import com.framework.http.result.HttpResult
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable


/**
 * @author: xiaxueyi
 * @date: 2022-08-18
 * @time: 17:27
 * @说明:  网络请求Observer(观察者),事件处理。当被观察发送数据流，通过订阅，在观察者处理接口，回调回去
 */
open class HttpObserver<T> : Observer<T>, LifecycleObserver,IDisposableCancel{
    /*请求标识*/
    private var mDisposableTag: String? = null

    private var mHttpResult: HttpResult<*>? = null

     constructor(disposableTag: String?, httpResult: HttpResult<*>?, lifecycleOwner: LifecycleOwner?) {
        mHttpResult = httpResult
        setDisposableTag(disposableTag)
        bindLifecycleOwner(lifecycleOwner)
    }

    override fun onSubscribe(d: Disposable) {
        DisposableManager.get()!!.addDisposable(mDisposableTag, d)
    }

    override fun onNext(value: T) {
        DisposableManager.get()!!.removeDisposable(mDisposableTag)
        if (mHttpResult != null) {
            mHttpResult!!.onSuccess(value as String)
        }
    }


    override fun onComplete() {}

    override fun onError(e: Throwable) {
        e.printStackTrace()
        DisposableManager.get()!!.removeDisposable(mDisposableTag)
        if (mHttpResult != null) {
            val exception = e as EventException
            mHttpResult!!.onError(exception.code, exception.msg)
        }
    }


    /**
     * 手动取消请求/在组件生命周期销毁时自动取消(只有绑定了LifecycleOwner才会自动回调)
     */

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public override fun cancel() {
        //如果没有移除，此时走取消逻辑
        if (!isCanceled) {
            mHttpResult?.onCancel()
        }
        DisposableManager.get()!!.removeDisposable(mDisposableTag)
    }

    override val isCanceled: Boolean
        get() {
            return DisposableManager.get()!!.isDisposed(mDisposableTag)
        }

    /**
     * 绑定生命周期
     */
    private fun bindLifecycleOwner(lifecycleOwner: LifecycleOwner?) {
        lifecycleOwner?.lifecycle?.addObserver(this)
    }

    /**
     * 设置请求唯一标识
     */
    private fun setDisposableTag(disposableTag: String?) {
        if (TextUtils.isEmpty(disposableTag)) {
            mDisposableTag = System.currentTimeMillis().toString()
        }else{
            mDisposableTag =  disposableTag
        }
    }


}