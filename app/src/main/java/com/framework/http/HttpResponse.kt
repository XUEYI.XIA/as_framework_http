package com.framework.http

import java.io.Serializable


/**
 * @author: xiaxueyi
 * @date: 2022-08-22
 * @time: 10:13
 * @说明:
 *  HttpCallback<T>
 * 1.实现接口 BaseResponse 重写函数，告知 http 框架 code == 0 认为是接口逻辑成功，code !=0 情况由 http 直接回调 onError
 * 2.如果不想实现 BaseResponse，则可以传任意类型到 HttpCallback<T>
 *
 */
class HttpResponse<T> : Serializable, BaseResponse {
    /*
    {
        "data": {},
        "errorCode": 0,
        "errorMsg": ""
    }
    */
    //
    //    /**
    //     * 描述信息
    //     */
    //    @SerializedName("msg")
    //    private String msg;
    //
    //    /**
    //     * 状态码
    //     */
    //    @SerializedName("code")
    //    private Object code;
    //
    //    /**
    //     * 数据对象/成功返回对象
    //     */
    //    @SerializedName("data")
    //    private T data;


    override var isSuccess = false
        get() = field
        set

    override var code: Any? = null //一般使用 int ，防止带有特殊字符，所以使用Object(Any)
        get() = field
        set


    override val msg: String? = null
        get() = if (field == null) "" else field!!
    var data: T? = null
        private set

    fun setData(data: T) {
        this.data = data
    }
}