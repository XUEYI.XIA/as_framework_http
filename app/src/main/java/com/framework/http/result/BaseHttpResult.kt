package com.framework.http.result

/**
 * @author: xiaxueyi
 * @date: 2022-08-18
 * @time: 14:28
 * @说明: 结果回调
 */
interface BaseHttpResult {

    fun onSuccess(data: String?)

    fun onError(code: Any?, msg: String?)

    fun onCancel()
}