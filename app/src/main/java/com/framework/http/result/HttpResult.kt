package com.framework.http.result

import android.util.Log
import com.framework.http.BaseResponse
import com.framework.http.exception.ExceptionEngine
import com.framework.http.parse.BaseParseHelper
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONObject
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type


/**
 * @author: xiaxueyi
 * @date: 2022-08-18
 * @time: 15:28
 * @说明: 结果h回调
 */
 open class HttpResult<T>constructor (httpCallback: HttpCallback<T>) : BaseHttpResult, BaseParseHelper {

    private var mHttpCallback: HttpCallback<T>?=null


    init {
        mHttpCallback = httpCallback
    }



    /**
     *
     */
    override fun onSuccess(data: String?) {
        try {
            parse(data)
        } catch (e: Exception) {
            Log.e("onError", e.toString())
            onError(ExceptionEngine.code_error_analytic, ExceptionEngine.msg_error_analytic)

        }
    }

    override fun onError(code: Any?, msg: String?) {
        mHttpCallback?.onError(code, msg)
    }

    override fun onCancel() {
        mHttpCallback?.onCancel()
    }

    /**
     * 开始解析数据
     */
    @Throws(Exception::class)
    override fun parse(data: String?) {
        if (mHttpCallback != null) {
            val genericType = getGenericType(mHttpCallback!!)
            val result: T? = parseResponse(data, genericType)
            if (result is BaseResponse) { //开发者实现 BaseResponse
                val response: BaseResponse = result as BaseResponse
                if (response.isSuccess) {
                    mHttpCallback!!.onSuccess(result)
                } else {
                    mHttpCallback!!.onError(response.code, response.msg)
                }
            } else { //开发者未实现 BaseResponse
                if (result != null) {
                    mHttpCallback!!.onSuccess(result)
                }
            }
        }
    }

    /**
     * 解析 Response
     * 特殊处理 String.class/JSONObject.class/JSONArray.class
     * 其他情况  fromJson(String json, Type typeOfT) (Type/Class)
     */
    @Throws(Exception::class)
    private fun <T> parseResponse(data: String?, rawType: Type): T? {
        return if (rawType === String::class.java) {
            data as T
        } else if (rawType === JSONObject::class.java) {
            data?.let { JSONObject(it) } as T
        } else if (rawType === JSONArray::class.java) {
            JSONArray(data) as T
        } else {
            return Gson().fromJson(data, rawType)
        }
    }

    /**
     * 获取泛型 Type
     *
     * @param httpCallback
     * @param <T>
     * @return
    </T> */
    private fun <T> getGenericType(httpCallback: HttpCallback<T>): Type {
        val types: Array<Type> = httpCallback.javaClass.genericInterfaces
        val params = (types[0] as ParameterizedType).actualTypeArguments
        return params[0]
    }

    /**
     * 获取泛型 Type Name
     *
     * @param type
     */
    private fun getGenericTypeName(type: Type): String {
        val genericTAllName = type.toString()
        return genericTAllName.replace("<[^<>]*>".toRegex(), "")
    }


}