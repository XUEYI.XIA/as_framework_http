package com.framework.http.result



/**
 * @author: xiaxueyi
 * @date: 2022-08-18
 * @time: 14:28
 * @说明: 结果回调给用户，一个桥梁
 */
interface HttpCallback<T> {

    fun onSuccess(`object`: T)

    fun onError(code: Any?, msg: String?)

    fun onCancel()
}