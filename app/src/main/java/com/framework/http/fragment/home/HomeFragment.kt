package com.framework.http.fragment.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.framework.http.databinding.FragmentHomeBinding
import com.framework.http.enum.HttpMethod
import com.framework.http.interfac.HttpApi
import com.framework.http.result.HttpCallback
import com.framework.http.testbean.TestBean
import com.framework.http.utils.HttpRequestService
import org.json.JSONArray
import org.json.JSONObject

class HomeFragment : Fragment() {

    private val TAG="HomeFragment";

    private var _binding: FragmentHomeBinding? = null

    var parameter: MutableMap<String, Any> = mutableMapOf();

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val textBean:TextView=binding.textBean
        val textString:TextView=binding.textString
        val textJsonObject:TextView=binding.textJsonObject
        val textJsonArray:TextView=binding.textJsonArray
        val textShow:TextView=binding.textShow

        try {
//            val json: String? = context?.let { StringUtils.getAssetsFile(it, "test_json.json") }
//            var type=object :TypeToken<TestBean>(){}.type;
//        val result=Gson().fromJson(json,TestBean::class.java)
//            val result=Gson().fromJson<TestBean>(json,type)

//            textBean.text= result.stocks?.get(0)?.symbol.toString()+result.stocks?.get(0)

//            Toast.makeText(activity,result.stocks?.get(0)?.symbol.toString(),Toast.LENGTH_SHORT).show()
        }catch (E:Exception){

            println(E.message)
        }


        parameter.put("id","00700.HK")
        parameter.put("interval","1m")
        parameter.put("period","1D")


        textBean.setOnClickListener{
            HttpRequestService
                .Builder()
                .setParameter(parameter)
                .setApiUrl(HttpApi.test)
                .setMethod(HttpMethod.GET)
                .setLifecycle(this)
                .build()
                .execute(object : HttpCallback<TestBean> {

                override fun onSuccess(data: TestBean) {
                    Log.e(TAG,"输出的数据(onSuccess)${data}")
                    var stringBuilder :StringBuilder= StringBuilder()
                    for(stocks in data.stocks!!){
                        stringBuilder.append(stocks.symbol)
                        stringBuilder.append(stocks.pc)
                        stringBuilder.append(stocks.maxPeriod)
                        stringBuilder.append(stocks.data)
                    }
                    textShow.text=stringBuilder.toString()
                }

                override fun onError(code: Any?, msg: String?) {
                    Log.e(TAG,"输出的数据(onError)${code}${msg}")
                    textShow.text="输出的数据(onError)${code}${msg}"
                }

                override fun onCancel() {
                    Log.e(TAG,"输出的数据(onCancel)")
                }

            })

        }


        textString.setOnClickListener{


            HttpRequestService
                .Builder()
                .setParameter(parameter)
                .setApiUrl(HttpApi.test)
                .setMethod(HttpMethod.GET)
                .build().execute(object : HttpCallback<String> {

                override fun onSuccess(data: String) {
                    Log.e(TAG,"输出的数据(onSuccess)${data}")
                    textShow.text=data.toString()
                }

                override fun onError(code: Any?, msg: String?) {
                    Log.e(TAG,"输出的数据(onError)${code}${msg}")
                    textShow.text="输出的数据(onError)${code}${msg}"
                }

                override fun onCancel() {
                    Log.e(TAG,"输出的数据(onCancel)")
                }

            })

        }


        textJsonObject.setOnClickListener{
            HttpRequestService
                .Builder()
                .setParameter(parameter)
                .setApiUrl(HttpApi.test)
                .build().execute(object : HttpCallback<JSONObject> {

                override fun onSuccess(data: JSONObject) {
                    Log.e(TAG,"输出的数据(onSuccess)${data}")
                    textShow.text=data.toString()
                }

                override fun onError(code: Any?, msg: String?) {
                    Log.e(TAG,"输出的数据(onError)${code}${msg}")
                    textShow.text="输出的数据(onError)${code}${msg}"
                }

                override fun onCancel() {
                    Log.e(TAG,"输出的数据(onCancel)")
                }

            })
        }


        textJsonArray.setOnClickListener{
            HttpRequestService
                .Builder()
                .setParameter(parameter)
                .setApiUrl(HttpApi.test)
                .build().execute(object : HttpCallback<JSONArray> {

                override fun onSuccess(data: JSONArray) {
                    Log.e(TAG,"输出的数据(onSuccess)${data}")
                    textShow.text=data.toString()
                }

                override fun onError(code: Any?, msg: String?) {
                    Log.e(TAG,"输出的数据(onError)${code}${msg}")
                    textShow.text="输出的数据(onError)${code}${msg}"
                }

                override fun onCancel() {
                    Log.e(TAG,"输出的数据(onCancel)")
                }

            })
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}