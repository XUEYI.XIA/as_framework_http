package com.framework.http.function

import com.framework.http.exception.ExceptionEngine.handleException
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.functions.Function


/**
 * http结果处理函数
 *
 */
class HttpResultFunction<T > : Function<Throwable, Observable<T>> {
    override fun apply(throwable: Throwable): Observable<T> {
        return Observable.error(handleException(throwable))
    }
}
