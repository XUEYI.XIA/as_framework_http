package com.framework.http.function

import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import io.reactivex.rxjava3.functions.Function

/**
 * 服务器结果处理函数
 *
 */

class ServerResultFunction : Function<JsonElement,String>{

    @Throws(Exception::class)
    override fun apply(response: JsonElement): String {
        /*避免html文本被格式化*/
        return GsonBuilder().disableHtmlEscaping().create().toJson(response)
    }
}
