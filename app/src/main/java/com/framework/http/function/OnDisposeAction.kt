package com.framework.http.function

import com.framework.http.disposable.IDisposableCancel
import io.reactivex.rxjava3.functions.Action


class OnDisposeAction(private val disposableCancel: IDisposableCancel?) : Action {
    @Throws(Exception::class)
    override fun run() {
        //Dispose
        disposableCancel?.cancel()
    }
}