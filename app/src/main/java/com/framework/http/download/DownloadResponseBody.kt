package com.framework.http.download

import okhttp3.MediaType
import okhttp3.ResponseBody
import okio.*
import java.io.IOException

/**
 * 下载ResponseBody
 *
 */
class DownloadResponseBody(responseBody: ResponseBody?, downloadProgress: IDownloadProgress?) : ResponseBody() {
    private val responseBody: ResponseBody?
    private val downloadProgress: IDownloadProgress?
    private lateinit var bufferedSource: BufferedSource

    init {
        this.responseBody = responseBody
        this.downloadProgress = downloadProgress
    }

    override fun contentType(): MediaType? {
        return responseBody?.contentType()
    }

    override fun source(): BufferedSource {
        if (bufferedSource == null) {
            bufferedSource = responseBody?.let { source(it.source()).buffer() }!!
        }

        return bufferedSource
    }

    override fun contentLength(): Long {
        return responseBody?.contentLength() ?: 0
    }



    private fun source(source: Source): Source {
        return object : ForwardingSource(source) {
            var readBytesCount = 0L
            var totalBytesCount = 0L
            @Throws(IOException::class)
            override fun read(sink: Buffer, byteCount: Long): Long {
                val bytesRead: Long = super.read(sink, byteCount)
                // read() returns the number of bytes read, or -1 if this source is exhausted.
                readBytesCount += if (bytesRead != -1L) bytesRead else 0
                if (totalBytesCount == 0L) {
                    totalBytesCount = contentLength()
                }
//                LogUtils.d("download progress readBytesCount:$readBytesCount  totalBytesCount:$totalBytesCount callback:$downloadProgress")
                downloadProgress?.progress(readBytesCount, totalBytesCount)
                return bytesRead
            }
        }
    }
}