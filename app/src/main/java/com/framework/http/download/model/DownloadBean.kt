package com.framework.http.download.model

import com.framework.http.api.APIService
import com.framework.http.download.DownloadCallback
import com.litesuits.orm.db.annotation.Column
import com.litesuits.orm.db.annotation.Ignore
import com.litesuits.orm.db.annotation.PrimaryKey
import com.litesuits.orm.db.annotation.Table
import com.litesuits.orm.db.enums.AssignType
import java.io.Serializable

/**
 * 下载实体类
 * 备注:用户使用下载类需要继承此类
 *
 */
@Table("download")
class DownloadBean : Serializable {
    @PrimaryKey(AssignType.AUTO_INCREMENT)
    @Column("_id")
    private var id: Long = 0

    @Column("localUrl")
    private var localUrl //本地存储地址
            : String? = null

    @Column("serverUrl")
    private var serverUrl //下载地址
            : String? = null

    @Column("totalSize")
    private var totalSize //文件大小
            : Long = 0

    @Column("currentSize")
    private var currentSize //当前大小
            : Long = 0

    @Column("state")
    private var state = State.NONE //下载状态


    @Ignore
    private var apiService: APIService? = null//接口service

    @Ignore
    private var callback: DownloadCallback<*>? = null//回调接口

    fun DownloadBean() {}

    fun DownloadBean(url: String?) {
        setServerUrl(url)
    }

    fun DownloadBean(url: String?, callback: DownloadCallback<*>?) {
        setServerUrl(url)
        setCallback(callback)
    }

    /**
     * 枚举下载状态
     */
    enum class State {
        NONE,  //无状态
        WAITING,  //等待
        LOADING,  //下载中
        PAUSE,  //暂停
        ERROR,  //错误
        FINISH
        //完成
    }


    fun getId(): Long {
        return id
    }

    fun setId(id: Long) {
        this.id = id
    }

    fun getLocalUrl(): String? {
        return if (localUrl == null) "" else localUrl
    }

    fun setLocalUrl(localUrl: String?) {
        this.localUrl = localUrl
    }

    fun getServerUrl(): String? {
        return if (serverUrl == null) "" else serverUrl
    }

    fun setServerUrl(serverUrl: String?) {
        this.serverUrl = serverUrl
    }

    fun getTotalSize(): Long {
        return totalSize
    }

    fun setTotalSize(totalSize: Long) {
        this.totalSize = totalSize
    }

    fun getCurrentSize(): Long {
        return currentSize
    }

    fun setCurrentSize(currentSize: Long) {
        this.currentSize = currentSize
    }

    fun getState(): State? {
        return state
    }

    fun setState(state: State) {
        this.state = state
    }

    fun getApiService(): APIService? {
        return apiService
    }

    fun setApiService(apiService: APIService?) {
        this.apiService = apiService
    }

    fun getCallback(): DownloadCallback<*>? {
        return callback
    }

    fun setCallback(callback: DownloadCallback<*>?) {
        this.callback = callback
    }

    //重置
    fun reset() {
        setCurrentSize(0)
        setState(State.NONE)
    }
}