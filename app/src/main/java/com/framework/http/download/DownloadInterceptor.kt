package com.framework.http.download

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

/**
 * 通过Interceptor回调监听Response进度
 *
 */
class DownloadInterceptor(private val downloadProgress: IDownloadProgress) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val response: Response = chain.proceed(chain.request())
        return response.newBuilder()
            .body(DownloadResponseBody(response.body, downloadProgress))
            .build()
    }
}