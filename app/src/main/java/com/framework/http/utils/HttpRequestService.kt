package com.framework.http.utils

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import androidx.lifecycle.LifecycleOwner
import com.framework.http.api.APIService
import com.framework.http.enum.HttpMethod
import com.framework.http.interfac.NetworkRequestParamsListener
import com.framework.http.observe.HttpObservable
import com.framework.http.observe.HttpObserver
import com.framework.http.result.HttpCallback
import com.framework.http.result.HttpResult
import io.reactivex.rxjava3.core.Observable
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * @author: xiaxueyi
 * @date: 2022-08-18
 * @time: 17:21
 * @说明: 请求实例
 */
class HttpRequestService private constructor(builder: Builder)  {

    private val mContext: Context? = null

    /*请求方式*/
    private var method: HttpMethod? = null

    /*请求参数*/
    private var parameter: MutableMap<String, Any>? = null

    /*header*/
    private var header: MutableMap<String, Any>? = null

    /*HttpResult*/
    private var httpResult: HttpResult<*>? = null

    /*UploadResult*/
//    private val uploadResult: UploadResult? = null

    /*LifecycleOwner*/
    private var lifecycleOwner: LifecycleOwner? = null

    /*标识请求的TAG*/
    private var tag: String? = null

    /*文件map*/
    private var fileMap: MutableMap<String, File>? = null

    /*基础URL*/
    private var baseUrl: String? = null

    /*apiUrl*/
    private var apiUrl: String? = null

    /*String参数*/
    private var bodyString: String? = null

    /*是否强制JSON格式*/
    private var isJson = false

    /*超时时长*/
    private var timeout: Long = 0

    /*时间单位*/
    private var timeUnit: TimeUnit? = null

    /*HttpObserver便于取消*/
    private var httpObserver: HttpObserver<Any>? = null

    private var networkRequestParamsListener: NetworkRequestParamsListener? = null //参数回调接口

    /**
     * 初始化函数
     */
    init {
        parameter = builder.parameter
        header = builder.header
        tag = builder.tag
        fileMap = builder.fileMap
        baseUrl = builder.baseUrl
        apiUrl = builder.apiUrl
        isJson = builder.isJson
        bodyString = builder.bodyString
        method = builder.method
        timeout = builder.timeout
        timeUnit = builder.timeUnit
        lifecycleOwner = builder.lifecycleOwner
    }


    /**
     * 执行普通Http请求
     */
    fun <T> execute(httpCallback: HttpCallback<T>?) {
        if (httpCallback == null) {
            throw NullPointerException("HttpCallback must not null!")
        } else {
            httpResult = HttpResult(httpCallback)
            doRequest()
        }
    }


    /**
     * 执行请求
     */
    private fun doRequest() {

        /**
         * header处理
         */
        disposeHeader()

        /**
         * Parameter处理
         */
        disposeParameter()

        /**
         * 请求方式处理
         */
        val apiObservable: Observable<Any>? = disposeApiObservable()

        /**
         * 构造 观察者
         */
        httpObserver = HttpObserver(tag, httpResult, lifecycleOwner)

        /**
         * 构造 被观察者
         */
        val httpObservable: HttpObservable = HttpObservable(apiObservable, httpObserver)

        /**
         * 设置监听，被观察和观察者订阅
         */
        httpObservable.observe()
    }


    /*处理ApiObservable*/
    private fun disposeApiObservable(): Observable<Any> ?{
        var apiObservable: Observable<Any>? = null

        /*是否JSON格式提交参数*/
        val hasBodyString = !TextUtils.isEmpty(bodyString)
        var requestBody: RequestBody? = null
        if (hasBodyString) {
            val mediaType : String;
                if (isJson) {
                    mediaType="application/json;charset=UTF-8";
                }else{
                    mediaType="text/plain;charset=utf-8"
                }
            requestBody = RequestBody.create(mediaType.toMediaTypeOrNull(), bodyString!!)
        }


        /*Api接口*/
        val apiService =
            RetrofitUtils.get()!!.getRetrofit(getContext(), getBaseUrl(), getTimeout(), getTimeUnit(),getNetworkRequestParamsListener())
                ?.create(APIService::class.java)


        /*未指定默认POST*/
        if (method == null) {
            method = HttpMethod.GET
        }

        when (method) {
            HttpMethod.GET ->{
                if (apiService != null) {
                    apiObservable = apiService.get(disposeApiUrl(), parameter, header)
                }
            }

            HttpMethod.POST ->{
                if (hasBodyString){
                    if (apiService != null) {
                        apiObservable = apiService.post(disposeApiUrl(), requestBody, header)
                    }

                } else{
                    if (apiService != null) {
                        apiObservable = apiService.post(disposeApiUrl(), parameter, header)
                    }
                }
            }

            HttpMethod.DELETE -> {
                if (apiService != null) {
                    apiObservable = apiService.delete(disposeApiUrl(), parameter, header)
                }

            }

            HttpMethod.PUT -> {
                if (apiService != null) {
                    apiObservable = apiService.put(disposeApiUrl(), parameter, header)
                }

            }

            else -> {
                TODO("UNKNOW METHOD")
            }
        }
        return apiObservable
    }


    /*ApiUrl处理*/
    private fun disposeApiUrl(): String {
        if (TextUtils.isEmpty(apiUrl)){
            apiUrl="";
        }
        return apiUrl as String;
    }


    /*处理Header*/
    private fun disposeHeader() {

        /*header空处理*/
        if (header == null) {
            header = mutableMapOf()
        }

        //添加基础 Header
        val baseHeader: MutableMap<String, Any> = Configure.get().baseHeader
        if (baseHeader.isNotEmpty()) {
            header!!.putAll(baseHeader)
        }
        if (header!!.isNotEmpty()) {
            //处理header中文或者换行符出错问题
            for (key in header!!.keys) {
                header!![key] = RequestUtils.getHeaderValueEncoded(header!![key])
            }
        }
    }

    /**
     * 处理 Parameter （请求参数）
     */
    private fun disposeParameter() {

        /*空处理*/
        if (parameter == null) {
            parameter = mutableMapOf()
        }
        //添加基础 Parameter
        val baseParameter: Map<String, Any>? = Configure.get().baseParameter ;
        if (baseParameter != null) {
            if (baseParameter.isNotEmpty()) {
                parameter!!.putAll(baseParameter)
            }
        }
    }


    /**
     * Configure配置
     */
    class Configure() {

        /*请求基础路径*/
       private var baseUrl: String? = null

        /*超时时长*/
        var timeout: Long = 60

        /*时间单位*/
        var timeUnit: TimeUnit

        /*Context*/
        /*全局上下文*/
        var context: Context? = null

        /*Handler*/
        /*全局Handler*/
        private var handler: Handler? = null

        /*请求参数*/
        var baseParameter: MutableMap<String, Any>? = null

        /*header*/
        private var header: MutableMap<String, Any>? = null

        /*是否显示Log*/
        var isShowLog: Boolean


        var networkRequestParamsListener: NetworkRequestParamsListener? = null //参数回调接口


        init {
            //默认60秒
            timeUnit = TimeUnit.SECONDS //默认秒
            isShowLog = true //默认打印LOG
        }

        private object Holder {
            @SuppressLint("StaticFieldLeak")
            val holder = Configure()
        }

        /*请求基础路径*/
        fun baseUrl(baseUrl: String?): Configure {
            this.baseUrl = baseUrl
            return this
        }

        fun getBaseUrl(): String ?{
            return baseUrl
        }

        /**
         * 设置基础参数
         */
        fun setBaseParameter(parameter: MutableMap<String, Any>?): Configure {
            baseParameter = parameter
            return this
        }

        /*基础Header*/
        fun baseHeader(header: MutableMap<String, Any>?): Configure {
            this.header = header
            return this
        }

        val baseHeader: MutableMap<String, Any>
            get() = if (header == null) TreeMap() else header!!

        /*超时时长*/
        fun timeout(timeout: Long): Configure {
            this.timeout = timeout
            return this
        }

        /*是否显示LOG*/
        fun showLog(showLog: Boolean): Configure {
            isShowLog = showLog
            return this
        }

        /*时间单位*/
        fun timeUnit(timeUnit: TimeUnit): Configure {
            this.timeUnit = timeUnit
            return this
        }

        /**请求头监听 */
        fun setNetworkRequestParamsListener(networkRequestParamsListener: NetworkRequestParamsListener?): Configure {
            this.networkRequestParamsListener = networkRequestParamsListener
            return this
        }

        /*初始化全局上下文*/
        fun init(app: Application): Configure {
            context = app
            handler = Handler(Looper.getMainLooper())
            return this
        }

        companion object {
            fun get(): Configure {
                return Holder.holder
            }
        }


    }


    /**
     * Builder
     * 构造Request所需参数，按需设置
     */
    class Builder {

        private var mContext: Context? = null

        var method:HttpMethod?=null;

        /*请求参数*/
        var parameter: MutableMap<String, Any>? = null

        /*header*/
        var header: MutableMap<String, Any>? = null

        /*LifecycleOwner*/
        var lifecycleOwner: LifecycleOwner? = null

        /*标识请求的TAG*/
        var tag: String? = null

        /*文件map*/
        var fileMap: MutableMap<String, File>? = null

        /*基础URL*/
        var baseUrl: String? = null

        /*apiUrl*/
        var apiUrl: String? = null

        /*String参数*/
        var bodyString: String? = null

        /*是否强制JSON格式*/
        var isJson = false

        /*超时时长*/
        var timeout: Long = 0

        /*时间单位*/
        var timeUnit: TimeUnit? = null

        var networkRequestParamsListener: NetworkRequestParamsListener? = null //参数回调接口


        fun setContext(context: Context?): HttpRequestService.Builder? {
            mContext = context
            return this
        }


        fun setMethod(method: HttpMethod):HttpRequestService.Builder{
            this.method=method;
            return this;
        }

        /**
         * GET
         */
        fun get():HttpRequestService.Builder{
            this.method=HttpMethod.GET
            return  this;
        }

        /**
         * POST
         */
        fun post():Builder{
            this.method=HttpMethod.POST;
            return this
        }

        /*DELETE*/
        fun delete(): Builder {
            method = HttpMethod.DELETE
            return this
        }

        /*PUT*/
        fun put(): Builder {
            method = HttpMethod.PUT
            return this
        }

        /*基础URL*/
        fun baseUrl(baseUrl: String?): Builder {
            this.baseUrl = baseUrl
            return this
        }

        /*API URL*/
        fun setApiUrl(apiUrl: String?): Builder {
            this.apiUrl = apiUrl
            return this
        }

        /* 增加 Parameter 不断叠加参数 包括基础参数 */
        fun addParameter(parameter: Map<String, Any>?): Builder {
            if (this.parameter == null) {
                this.parameter = TreeMap()
            }
            this.parameter!!.putAll((parameter)!!)
            return this
        }


        /**
         * 设置参数 ,设置 Parameter 会覆盖 Parameter 包括基础参数
         */
        fun setParameter(parameter: MutableMap<String, Any>):HttpRequestService.Builder{
            this.parameter=parameter;
            return this
        }

        /* 设置String 类型参数  覆盖之前设置  isJson:是否强制JSON格式    bodyString设置后Parameter则无效 */
        fun setBodyString(bodyString: String?, isJson: Boolean): Builder {
            this.isJson = isJson
            this.bodyString = bodyString
            return this
        }

        /* 增加 Header 不断叠加 Header 包括基础 Header */
        fun addHeader(header: MutableMap<String, Any>?): Builder {
            if (this.header == null) {
                this.header = sortedMapOf()
            }
            this.header!!.putAll((header)!!)
            return this
        }

        /*设置 Header 会覆盖 Header 包括基础参数*/
        fun setHeader(header: MutableMap<String, Any>?): Builder {
            this.header = header
            return this
        }

        /*LifecycleProvider*/
        fun setLifecycle(lifecycleOwner: LifecycleOwner?): Builder {
            this.lifecycleOwner = lifecycleOwner
            return this
        }

        /*tag*/
        fun tag(tag: String?): Builder {
            this.tag = tag
            return this
        }

        /*文件集合*/
        fun file(file: MutableMap<String, File>?): Builder {
            fileMap = file
            return this
        }

        /*一个Key对应多个文件*/
        fun file(key: String, fileList: List<File>): Builder {
            if (fileMap == null) {
                fileMap = IdentityHashMap()
            }
            if (fileList.isNotEmpty()) {
                for (file: File in fileList) {
                    fileMap!!.put(key, file)
                }
            }
            return this
        }

        /*超时时长*/
        fun timeout(timeout: Long): Builder {
            this.timeout = timeout
            return this
        }

        /*时间单位*/
        fun timeUnit(timeUnit: TimeUnit?): Builder {
            this.timeUnit = timeUnit
            return this
        }

        /**
         * 设置参数事件监听
         * @param networkRequestParamsListener
         * @return
         */
        fun setNetworkRequestParamsListener(networkRequestParamsListener: NetworkRequestParamsListener?): Builder {
            this.networkRequestParamsListener = networkRequestParamsListener
            return this
        }

        fun build(): HttpRequestService {
            return HttpRequestService(this)
        }


    }


    /**
     * 获取上下文对象
     * @return
     */
    private fun getContext(): Context? {
        //如果没有重新指定URL则是用默认配置
        return mContext ?: Configure.get().context
    }


    /**
     * 获取基础URL
     */
    private fun getBaseUrl(): String? {
        //如果没有重新指定URL则是用默认配置
        return if (TextUtils.isEmpty(baseUrl)) Configure.get().getBaseUrl() else baseUrl
    }

    /**
     * 获取超时时间
     */
    private fun getTimeout(): Long {
        //当前请求未设置超时时间则使用全局配置
        return if (timeout == 0L) Configure.get().timeout else timeout
    }

    /**
     * 获取超时时间单位
     */
    private fun getTimeUnit(): TimeUnit? {
        //当前请求未设置超时时间单位则使用全局配置
        return if (timeUnit == null) Configure.get().timeUnit else timeUnit
    }


    /**
     * 获取请求头参数回调
     */
    private fun getNetworkRequestParamsListener():NetworkRequestParamsListener?{
        return networkRequestParamsListener ?: Configure.get().networkRequestParamsListener
    }

}