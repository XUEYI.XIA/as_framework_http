package com.framework.http.utils

import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type


/**
 * @author: xiaxueyi
 * @date: 2022-08-23
 * @time: 16:45
 * @说明:
 */
class ParameterizedTypeAdapter(private val mRawType: Type, private val typeArgument: Type) : ParameterizedType {

    override fun getActualTypeArguments(): Array<Type> = arrayOf(typeArgument)

    override fun getRawType(): Type = mRawType

    override fun getOwnerType(): Type? = null
}