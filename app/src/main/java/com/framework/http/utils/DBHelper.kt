package com.framework.http.utils

import android.content.Context
import com.litesuits.orm.LiteOrm
import com.litesuits.orm.db.DataBaseConfig

/**
 * 数据库辅助类
 *
 * @https://github.com/litesuits/android-lite-orm
 */
class DBHelper private constructor() {
    /*数据库名称*/
    private var DB_NAME = "com_mvvm" //"com_mvvm.db"
    private val DB_VERSION = 1
    private val context: Context

    init {
        if (HttpRequestService.Configure.get().context== null) {
            throw NullPointerException("RHttp not init!")
        }
        context = HttpRequestService.Configure.get().context!!
        DB_NAME = DB_NAME + context.packageName + ".db"
        initDB(context)
    }

    /**
     * 初始化数据库
     *
     * @param context
     */
    private fun initDB(context: Context) {
        if (db == null) {
            val config = DataBaseConfig(context, DB_NAME)
//            config.debugged = HttpRequestService.Configure.get().isShowLog() // open the log
            config.dbVersion = DB_VERSION // set database version
            config.onUpdateListener = null // set database update listener
            db = LiteOrm.newSingleInstance(config)
        }
    }

    /**
     * 插入或者更新对象
     * 备注:有则更新，无则插入
     *
     * @param object
     * @return
     */
    fun insertOrUpdate(`object`: Any?): Long {
        var count: Long = 0
        if (db != null) {
            count = db!!.save(`object`)
        }
        return count
    }

    /**
     * 删除对象
     *
     * @param var1
     * @return
     */
    fun delete(var1: Any?): Int {
        var count = 0
        if (db != null) {
            count = db!!.delete(var1)
        }
        return count
    }

    /**
     * 查询数据总数
     *
     * @param var1
     * @param <T>
     * @return
    </T> */
    fun <T> queryCount(var1: Class<T>?): Long {
        var count: Long = 0
        if (db != null) {
            count = db!!.queryCount<T>(var1)
        }
        return count
    }

    /**
     * 查询列表
     *
     * @param var1
     * @param <T>
     * @return
    </T> */
    fun <T> query(var1: Class<T>?): ArrayList<T> {
        var list = ArrayList<T>()
        if (db != null) {
            list = db!!.query<T>(var1)
        }
        return list
    }

    /**
     * 根据ID查询数据
     *
     * @param var1
     * @param var2
     * @param <T>
     * @return
    </T> */
    fun <T> queryById(var1: Long, var2: Class<T>?): T? {
        var t: T? = null
        if (db != null) {
            t = db!!.queryById<T>(var1, var2)
        }
        return t
    }

    companion object {
        private var instance: DBHelper? = null
        private var db: LiteOrm? = null
        fun get(): DBHelper? {
            if (instance == null) {
                synchronized(DBHelper::class.java) {
                    if (instance == null) {
                        instance = DBHelper()
                    }
                }
            }
            return instance
        }
    }
}