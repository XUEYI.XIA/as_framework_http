package com.framework.http.utils

import android.content.Context
import android.util.Log
import com.framework.http.interceptor.BaseInterceptor
import com.framework.http.interfac.NetworkRequestParamsListener
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLSession


/**
 * @author: xiaxueyi
 * @date: 2022-08-18
 * @time: 15:47
 * @说明: OkHttpClient 工具类
 */
class OkHttpClientUtils{

    private val TAG="OkHttpClientUtils";

    private var okHttpClient :OkHttpClient.Builder = OkHttpClient.Builder(); //okHttp 对象


    /**
     * 获取Retrofit对象,带请求头监听
     * @param baseUrl
     * @param timeOut
     * @param timeUnit
     * @param interceptorArray
     * @param networkRequestParamsListener
     * @return
     */
    fun getOkHttpClientBase(context: Context?, baseUrl:String?, timeOut:Long?, timeUnit: TimeUnit?, networkRequestParamsListener: NetworkRequestParamsListener?):OkHttpClient{
        //超时设置
        if (timeUnit != null) {
            if (timeOut != null) {
                okHttpClient
                    .readTimeout(timeOut,timeUnit)
                    .writeTimeout(timeOut,timeUnit)
            }
        }

        /**
         * https设置
         * 备注:信任所有证书,不安全有风险
         */
        val sslParams = HttpsUtils.getSslSocketFactory()
        sslParams.sSLSocketFactory?.let { sslParams.trustManager?.let { it1 ->
            okHttpClient.sslSocketFactory(it,
                it1
            )
        } }
        /**
         * 配置https的域名匹配规则，不需要就不要加入，使用不当会导致https握手失败
         * 备注:google平台不允许直接返回true
         */
        okHttpClient.hostnameVerifier(object : HostnameVerifier {

            override fun verify(hostname: String?, session: SSLSession?): Boolean {
                return true
            }
        });

        //日志拦截器
        val httpLoggingInterceptor : HttpLoggingInterceptor = HttpLoggingInterceptor(object :HttpLoggingInterceptor.Logger{
            override fun log(message: String) {
                Log.e(TAG,"okHttp-->>:\t$message")
            }
        })
        //设置日志等级
        httpLoggingInterceptor.level=HttpLoggingInterceptor.Level.BODY

        //网络请求拦截器
        val httpInterceptor = Interceptor { chain ->
            val request = chain.request()

            val response: Response

             try {
                 response = chain.proceed(request)
            } catch (e: Exception) {
                 Log.e(TAG,"Exception--->>:\t"+e.message)
                throw e
            }
            response
        }

        //基类拦截器
        val baseInterceptor = BaseInterceptor(networkRequestParamsListener)

        okHttpClient.addInterceptor(httpInterceptor)
        okHttpClient.addInterceptor(httpLoggingInterceptor)
        okHttpClient.addInterceptor(baseInterceptor)


        return okHttpClient.build();

    }

}