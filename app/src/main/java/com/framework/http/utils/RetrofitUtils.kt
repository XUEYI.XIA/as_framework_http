package com.framework.http.utils

import android.content.Context
import com.framework.http.interfac.NetworkRequestParamsListener
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit


/**
 * @author: xiaxueyi
 * @date: 2022-08-18
 * @time: 14:55
 * @说明: retrofit 工具类，单独封装成一个工具类，解耦代码
 */
open class RetrofitUtils private constructor(){

    companion object{

        private const val mDateFormat:String="yyyy-MM-dd HH:mm:ss";

        private var instance: RetrofitUtils?=null; //retrofit单例对象

        private var retrofit: Retrofit.Builder? = null //Retrofit 建造者对象


        /**
         * 获取单例
         */
        fun get():RetrofitUtils?{
            if(instance==null){
                synchronized(RetrofitUtils::class.java){
                    if(instance==null){
                        instance= RetrofitUtils();
                    }
                }
            }
            return instance
        }
    }


    /**
     * 初始化Retrofit 对象，建造者模式
     */
    init {
        retrofit=Retrofit.Builder();
    }


    /**
     * 获取Retrofit对象,普通方法
     * @param baseUrl
     * @param timeOut
     * @param timeUnit
     * @return
     */
    fun getRetrofit(context: Context?, baseUrl:String?,timeOut:Long?,timeUnit: TimeUnit?):Retrofit?{
       return getRetrofit(context,baseUrl,timeOut,timeUnit,null)
    }


    /**
     * 获取Retrofit对象,带请求头监听，没有拦截器
     * @param baseUrl
     * @param timeOut
     * @param timeUnit
     * @param networkRequestParamsListener
     * @return
     */
    fun getRetrofit(context: Context?, baseUrl:String?,timeOut:Long?,timeUnit: TimeUnit?,networkRequestParamsListener: NetworkRequestParamsListener?):Retrofit?{
        return getRetrofit(context,baseUrl,timeOut,timeUnit,null,networkRequestParamsListener)
    }

    /**
     * 获取Retrofit对象,带请求头监听
     * @param baseUrl
     * @param timeOut
     * @param timeUnit
     * @param interceptorArray
     * @param networkRequestParamsListener
     * @return
     */
   open fun getRetrofit(context: Context?, baseUrl:String?,timeOut:Long?,timeUnit: TimeUnit?, interceptorArray:IntIterator?,networkRequestParamsListener: NetworkRequestParamsListener?):Retrofit?{

        //创建json
        val gson:Gson=GsonBuilder()
            .setDateFormat(mDateFormat)
            .serializeNulls()
            .setPrettyPrinting()
            .disableHtmlEscaping()
            .create();

        //创建OkHttpClient 对象
        val client:OkHttpClient=OkHttpClientUtils().getOkHttpClientBase(context,baseUrl,timeOut,timeUnit,networkRequestParamsListener)

        if (baseUrl != null) {
            retrofit
                ?.client(client)
                ?.baseUrl(baseUrl)
                ?.addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                ?.addConverterFactory(ScalarsConverterFactory.create())
                ?.addConverterFactory(GsonConverterFactory.create(gson))
        };
        return retrofit!!.build();
    }

}