package com.framework.http.utils

import android.content.Context
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader


/**
 * @author: xiaxueyi
 * @date: 2022-08-23
 * @time: 14:12
 * @说明:
 */
object StringUtils {


    /**
     * 从Assets读取JSON文件
     * @param context
     * @param fileName
     * @return
     */
    fun getAssetsFile(context: Context, fileName: String?): String? {
        val stringBuilder = StringBuilder()
        try {
            val assetManager = context.assets
            val bf = BufferedReader(
                InputStreamReader(
                    assetManager.open(
                        fileName!!
                    )
                )
            )
            var line: String?
            while (bf.readLine().also { line = it } != null) {
                stringBuilder.append(line)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return stringBuilder.toString()
    }
}