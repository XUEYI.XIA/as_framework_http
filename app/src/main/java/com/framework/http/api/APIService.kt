package com.framework.http.api

import io.reactivex.rxjava3.core.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.http.*

/**
 * @author: xiaxueyi
 * @date: 2022-08-18
 * @time: 14:28
 * @说明: api 接口，所有类型使用集合
 */
interface APIService {


    /**
     * GET 请求
     *
     * @param url       api接口url
     * @param parameter 请求参数map
     * @param header    请求头map
     * @return
     */
    @GET
    fun get(@Url url: String?, @QueryMap parameter: MutableMap<String, Any>?, @HeaderMap header: MutableMap<String, Any>?): Observable<Any>?



    /**
     * POST 请求
     *
     * @param url       api接口url
     * @param parameter 请求参数map
     * @param header    请求头map
     * @return
     */
    @FormUrlEncoded
    @POST
    fun post(@Url url: String?, @FieldMap parameter: MutableMap<String, Any>?, @HeaderMap header: MutableMap<String, Any>?): Observable<Any>?


    /**
     * @param requestBody 用于String/JSON格式数据
     */
    @POST
    fun post(@Url url: String?, @Body requestBody: RequestBody?, @HeaderMap header: MutableMap<String, Any>?): Observable<Any>?


    /**
     * DELETE 请求
     *
     * @param url       api接口url
     * @param parameter 请求参数map
     * @param header    请求头map
     * @return
     */
    @DELETE
    fun delete(
        @Url url: String?,
        @QueryMap parameter: Map<String, Any>?,
        @HeaderMap header: Map<String, Any>?
    ): Observable<Any>?

    /**
     * PUT 请求
     *
     * @param url       api接口url
     * @param parameter 请求参数map
     * @param header    请求头map
     * @return
     */
    @FormUrlEncoded
    @PUT
    fun put(
        @Url url: String?,
        @FieldMap parameter: Map<String, Any>?,
        @HeaderMap header: Map<String, Any>?
    ): Observable<Any>?

    /**
     * 多文件上传
     *
     * @param url       api接口url
     * @param parameter 请求接口参数
     * @param header    请求头map
     * @param fileList  文件列表
     * @return
     * @Multipart 文件上传注解 multipart/form-data
     */
    @Multipart
    @POST
    fun upload(
        @Url url: String?,
        @PartMap parameter: Map<String, Any>?,
        @HeaderMap header: Map<String, Any>?,
        @Part fileList: List<MultipartBody.Part>?
    ): Observable<Any>

    /**
     * 断点续传下载
     *
     * @param range  断点下载范围 bytes= start - end
     * @param url    下载地址
     * @param header 请求头map
     * @return
     * @Streaming 防止内容写入内存, 大文件通过此注解避免OOM
     */
    @Streaming
    @GET
    fun download(
        @Header("RANGE") range: String?,
        @Url url: String?,
        @HeaderMap header: Map<String, Any>?
    ): Observable<ResponseBody>


}

